To run different maps, do `./test_planner_2d ../data/[yaml file]` and `eog test_planner_2d.jpg`.

Right now I'm having issues with anything below `narrow_doorway_10cells.yaml`, even with very fine discretization. Tests show that getting through narrow doorways is possible with this planner, but may require fine discretization of primitives/timesteps. Runtime in 3d is also a concern.