#include "opencv_drawing.hpp"
#include "read_map.hpp"
#include "timer.hpp"
#include <mpl_planner/planner/map_planner.h>

/*
 * Draw primitives generated from different initial conditions
*/
int main(int argc, char **argv) {

  // Need a map to draw stuff
  if (argc != 2) {
    printf(ANSI_COLOR_RED "Input yaml required!\n" ANSI_COLOR_RESET);
    return -1;
  }

  // Load the map
  MapReader<Vec2i, Vec2f> reader(argv[1]);
  if (!reader.exist()) {
    printf(ANSI_COLOR_RED "Cannot find input file [%s]!\n" ANSI_COLOR_RESET,
           argv[1]);
    return -1;
  }

  std::shared_ptr<MPL::OccMapUtil> map_util =
      std::make_shared<MPL::OccMapUtil>();
  map_util->setMap(reader.origin(), reader.dim(), reader.data(),
                   reader.resolution());
  map_util->freeUnknown();

  // Initialize set of possible control inputs
  decimal_t u = 0.5;
  decimal_t du = u;
  vec_E<VecDf> U;
  for (decimal_t dx = -u; dx <= u; dx += du)
    for (decimal_t dy = -u; dy <= u; dy += du)
      U.push_back(Vec2f(dx, dy));

  decimal_t primitive_time = 2.5;

  // Make set of starting states
  Waypoint2D cur_setpoint;
  cur_setpoint.pos << 2.5, 0.0;
  cur_setpoint.vel = Vec2f::Zero();
  cur_setpoint.acc = Vec2f::Zero();
  cur_setpoint.jrk = Vec2f::Zero();
  cur_setpoint.yaw = 0;
  cur_setpoint.use_pos = true;
  cur_setpoint.use_vel = true;
  cur_setpoint.use_acc = true;
  cur_setpoint.use_jrk = false;
  cur_setpoint.use_yaw = false;

  std::vector<Waypoint<2>> starting_points;
  starting_points.push_back(cur_setpoint);

  cur_setpoint.pos << 5.5, 0.0;
  cur_setpoint.vel << 1.0, 0.0;
  starting_points.push_back(cur_setpoint);

  cur_setpoint.pos << 10.0, 0.0;
  cur_setpoint.acc << 1.0, 0.0;
  starting_points.push_back(cur_setpoint);

  // Generate primitives, draw them
  std::string file_name("draw_primitives");
  OpenCVDrawing opencv_drawing(map_util);
  for (const auto &sp : starting_points) {

    const unsigned int num_points = 50;
    for (const auto &u : U) {
      // std::cout << u.transpose() << std::endl;
      Primitive<2> pr(sp, u, primitive_time);
      vec_Vec2f pr_points;
      auto waypoints = pr.sample(num_points);
      for (const auto &wp : waypoints) {
        pr_points.push_back(wp.pos);
      }
      opencv_drawing.drawPoints(pr_points, black);
    }
  }

  if (OPENCV_WINDOW) {
    // show the plot
    opencv_drawing.show(file_name);
  } else {
    // save the plot
    opencv_drawing.save(file_name + ".jpg");
  }
}
